import Vue from 'vue'
import axios from 'axios'
import vueAxios from 'vue-axios'
import App from './App.vue'
import router from './router'
import store from './store'

import 'element-ui/lib/theme-chalk/index.css';
import 'ant-design-vue/dist/antd.css';
import 'vant/lib/index.css';

import './plugins/vant.js';
import '@/plugins/element.js'
import '@/plugins/antDesign.js'

Vue.prototype.$http = axios

Vue.config.productionTip = false

Vue.use(vueAxios, axios)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')